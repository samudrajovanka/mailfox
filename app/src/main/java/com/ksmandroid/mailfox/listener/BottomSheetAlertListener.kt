package com.ksmandroid.mailfox.listener

interface BottomSheetAlertListener {
    fun getUserChoices(yes: Boolean)
}