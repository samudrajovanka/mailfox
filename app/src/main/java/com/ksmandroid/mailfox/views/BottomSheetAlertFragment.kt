package com.ksmandroid.mailfox.views

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.ksmandroid.mailfox.R
import com.ksmandroid.mailfox.listener.BottomSheetAlertListener
import kotlinx.android.synthetic.main.bts_alert.*

class BottomSheetAlertFragment(private val listener: BottomSheetAlertListener) : BottomSheetDialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.bts_alert, container, false)
    }

    private var title = ""
    private var subtitle = ""

    fun setTitle(txt: String) {
        title = txt
    }

    fun setSubtitle(txt: String) {
        subtitle = txt
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tv_title_bts_alert.text = title
        tv_subtitle_bts_alert.text = subtitle

        btn_cancel_bts_alert.setOnClickListener {
            dismiss()
        }

        btn_ok_bts_alert.setOnClickListener {
            listener.getUserChoices(true)
            dismiss()
        }
    }

}