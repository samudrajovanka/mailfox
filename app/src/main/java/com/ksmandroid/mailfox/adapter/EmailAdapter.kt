package com.ksmandroid.mailfox.adapter

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.ksmandroid.mailfox.R
import com.ksmandroid.mailfox.model.Mail
import com.ksmandroid.mailfox.rest.MailFoxRestClient
import com.ksmandroid.mailfox.ui.allInbox.AllInboxFragment
import com.ksmandroid.mailfox.ui.main.MainActivity
import com.ksmandroid.mailfox.ui.splashscreen.SplashScreenActivity
import com.ksmandroid.mailfox.utils.getDataUser
import com.ksmandroid.mailfox.utils.getResponseErr
import com.loopj.android.http.AsyncHttpResponseHandler
import cz.msebera.android.httpclient.Header
import kotlinx.android.synthetic.main.item_email.view.*
import kotlinx.android.synthetic.main.nav_header_main.view.*
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.collections.ArrayList

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class EmailAdapter(
    private val listEmail: ArrayList<Mail?>
) : RecyclerView.Adapter<EmailAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_email, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = listEmail.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(listEmail[position]!!)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(email: Mail) {
            with(itemView) {

                MailFoxRestClient.get("/users/${email.from}", null, object : AsyncHttpResponseHandler() {
                    @RequiresApi(Build.VERSION_CODES.O)
                    @SuppressLint("SimpleDateFormat")
                    override fun onSuccess(
                        statusCode: Int,
                        headers: Array<out Header>,
                        responseBody: ByteArray
                    ) {
                        val result = String(responseBody)

                        try {
                            val responseObject = JSONObject(result)
                            val data = responseObject.getJSONObject("data")

                            val user = data.getJSONObject("user")
                            val fullName = user.getString("fullName")
                            val photoProfileObj = user.getJSONObject("photoProfile")

                            var photoProfile = ""
                            if(photoProfileObj.length() > 0) {
                                photoProfile = photoProfileObj.getString("path")
                            }

                            if (photoProfile == "") {
                                tv_item_char.visibility = View.VISIBLE
                                iv_item_email_photo_profile.visibility = View.INVISIBLE
                                print(fullName[0].toString())
                                tv_item_char.text = fullName[0].toString()
                            } else {
                                tv_item_char.visibility = View.INVISIBLE
                                iv_item_email_photo_profile.visibility = View.VISIBLE
                                Glide.with(context)
                                    .load(Uri.parse("https://fp-ksm-android.herokuapp.com/$photoProfile"))
                                    .into(iv_item_email_photo_profile)
                            }

                            tv_item_email_user.text = fullName
                            tv_item_subject_email.text = email.subject
                            tv_item_message_email.text = email.message

                            //val format = SimpleDateFormat()
                            //val date: Date = format.parse(email.sendDate)
                            val date = LocalDate.parse(email.sendDate, DateTimeFormatter.ISO_OFFSET_DATE_TIME)
                            val currentDate = LocalDate.now()

                            var dateString = ""
                            if (date.year == currentDate.year) {
                                dateString = "${date.dayOfMonth} ${date.month}"
                            } else {
                                dateString = "${date.dayOfMonth}/${date.monthValue}/${date.year}"
                            }

                            tv_date_item_email.text = dateString
                            cb_item_email_favorite.isChecked = email.favorite!!

                        } catch (e: Exception) {
                            Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show()
                        }
                    }

                    override fun onFailure(
                        statusCode: Int,
                        headers: Array<out Header>,
                        responseBody: ByteArray,
                        error: Throwable
                    ) {
                        val result = String(responseBody)

                        getResponseErr(context, result)
                    }

                })
            }
        }
    }
}