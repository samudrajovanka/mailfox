package com.ksmandroid.mailfox.rest

import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.AsyncHttpResponseHandler
import com.loopj.android.http.RequestParams

class MailFoxRestClient {
    companion object {
        private val BASE_URL = "https://fp-ksm-android.herokuapp.com/api"
        private val API_KEY = "mailfoxAPIKey0505211223"

        private val client = AsyncHttpClient()

        fun setHeader(header: String, value: String) {
            client.addHeader(header, value)
        }

        fun get(url: String, params: RequestParams?, responseHandler: AsyncHttpResponseHandler) {
            client.addHeader("X-API-KEY", API_KEY)
            client.get(getAbsoluteUrl(url), params, responseHandler)
        }

        fun post(url: String, params: RequestParams, responseHandler: AsyncHttpResponseHandler) {
            client.addHeader("X-API-KEY", API_KEY)
            client.post(getAbsoluteUrl(url), params, responseHandler)
        }

        private fun getAbsoluteUrl(relativeUrl: String): String {
            return BASE_URL + relativeUrl
        }
    }
}