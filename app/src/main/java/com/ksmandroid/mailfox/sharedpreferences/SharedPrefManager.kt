package com.ksmandroid.mailfox.sharedpreferences

import android.content.Context

class SharedPrefManager(val context: Context) {
    private val PREF_NAME = "com.ksmandroid.mailfox"
    private val LOGIN_STATE = "com.ksmandroid.mailfox.login"
    private val TOKEN_VALUE = "com.ksmandroid.mailfox.token"
    private val EMAIL_LOGIN = "com.ksmandroid.mailfox.email"

    private val pref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)

    fun saveStateLogin(state: Boolean) {
        pref.edit().putBoolean(LOGIN_STATE, state).apply()
    }

    fun isStateLogin() : Boolean {
        return pref.getBoolean(LOGIN_STATE, false)
    }

    fun saveToken(token: String) {
        pref.edit().putString(TOKEN_VALUE, token).apply()
    }

    fun getToken() : String? {
        return pref.getString(TOKEN_VALUE, "")
    }

    fun saveEmail(email: String) {
        pref.edit().putString(EMAIL_LOGIN, email).apply()
    }

    fun getEmail() : String? {
        return pref.getString(EMAIL_LOGIN, "")
    }
}