package com.ksmandroid.mailfox.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class User(
    val fullname: String? = "",
    val email: String? = "",
    val photoProfile: String? = "",
    val photoBanner: String? = "",
    val inboxMails: ArrayList<Mail?> = ArrayList(),
    val sentMails: ArrayList<Mail?> = ArrayList()
) : Parcelable