package com.ksmandroid.mailfox.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Mail (
    val from: String? = "",
    val to: String? = "",
    val subject: String? = "",
    val message: String? = "",
    val sendDate: String? = "",
    val read: Boolean? = false,
    val favorite: Boolean? = false
) : Parcelable