package com.ksmandroid.mailfox.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Photo(
    val filename: String? = "",
    val path: String? = "",
    val size: Int? = 0
) : Parcelable