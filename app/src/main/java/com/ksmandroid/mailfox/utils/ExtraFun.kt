package com.ksmandroid.mailfox.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.ksmandroid.mailfox.model.Mail
import com.ksmandroid.mailfox.model.User
import com.ksmandroid.mailfox.sharedpreferences.SharedPrefManager
import com.ksmandroid.mailfox.ui.login.LoginActivity
import org.json.JSONObject

private lateinit var pref: SharedPrefManager

fun EditText.clear() {
    this.text?.clear()
    this.clearFocus()
}

fun Fragment.hideKeyboard() {
    view?.let { activity?.hideKeyboard(it) }
}

fun Activity.hideKeyboard() {
    hideKeyboard(currentFocus ?: View(this))
}

fun Context.hideKeyboard(view: View) {
    val inputMethodManager =
        getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
}

fun getDataUser(user: JSONObject) : User {
    val nameRes = user.getString("fullName")
    val emailRes = user.getString("email")
    val photoProfileObj = user.getJSONObject("photoProfile")
    val photoBannerObj = user.getJSONObject("photoBanner")
    val inboxMailsRes = user.getJSONArray("inboxMails")
    val sentMailsRes = user.getJSONArray("sentMails")

    var photoProfile = ""
    var photoBanner = ""
    if(photoProfileObj.length() > 0) {
        photoProfile = photoProfileObj.getString("path")
    }

    if(photoBannerObj.length() > 0) {
        photoBanner = photoBannerObj.getString("path")
    }

    val inboxMails = arrayListOf<Mail?>()
    val sentMails = arrayListOf<Mail?>()

    for (i in 0 until inboxMailsRes.length()) {
        val inboxMailObj: JSONObject = inboxMailsRes.getJSONObject(i)
        val from = inboxMailObj.getString("from")
        val subject = inboxMailObj.getString("subject")
        val message = inboxMailObj.getString("message")
        val sendDate = inboxMailObj.getString("sendDate")
        val read = inboxMailObj.getBoolean("read")
        val favorite = inboxMailObj.getBoolean("favorite")
        inboxMails.add(Mail(from = from, subject = subject, message = message, sendDate = sendDate, read = read, favorite = favorite))
    }

    for (i in 0 until sentMailsRes.length()) {
        val sentMailObj: JSONObject = sentMailsRes.getJSONObject(i)
        val to = sentMailObj.getString("to")
        val subject = sentMailObj.getString("subject")
        val message = sentMailObj.getString("message")
        val sendDate = sentMailObj.getString("sendDate")
        val favorite = sentMailObj.getBoolean("favorite")
        sentMails.add(Mail(to = to, subject = subject, message = message, sendDate = sendDate, favorite = favorite))
    }

    return User(nameRes, emailRes, photoProfile, photoBanner, inboxMails, sentMails)
}

fun getResponseErr(context: Context, result: String) {
    if (result == "Unauthorized") {
        pref.saveToken("")
        pref.saveStateLogin(false)

        context.startActivity(Intent(context, LoginActivity::class.java))
    } else {
        try {
            val responseObject = JSONObject(result)
            val message = responseObject.getString("message")
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show()

        } catch (e: Exception) {
            Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show()
        }
    }
}