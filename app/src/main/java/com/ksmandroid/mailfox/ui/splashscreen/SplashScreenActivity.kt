package com.ksmandroid.mailfox.ui.splashscreen

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.ksmandroid.mailfox.R
import com.ksmandroid.mailfox.model.Mail
import com.ksmandroid.mailfox.model.User
import com.ksmandroid.mailfox.rest.MailFoxRestClient
import com.ksmandroid.mailfox.sharedpreferences.SharedPrefManager
import com.ksmandroid.mailfox.ui.allInbox.AllInboxFragment
import com.ksmandroid.mailfox.ui.login.LoginActivity
import com.ksmandroid.mailfox.ui.main.MainActivity
import com.ksmandroid.mailfox.utils.getDataUser
import com.ksmandroid.mailfox.utils.getResponseErr
import com.loopj.android.http.AsyncHttpResponseHandler
import cz.msebera.android.httpclient.Header
import kotlinx.android.synthetic.main.activity_splash_screen.*
import org.json.JSONObject

class SplashScreenActivity : AppCompatActivity() {
    private lateinit var pref: SharedPrefManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        pref = SharedPrefManager(this)

        // menghilangkan toolbar saat splashscreen
        supportActionBar?.hide()

        val rightAnim = AnimationUtils.loadAnimation(this, R.anim.right_animation)
        val leftAnim = AnimationUtils.loadAnimation(this, R.anim.left_animation)

        iv_logo_fox.animation = leftAnim
        iv_logo_text.animation = rightAnim

        val splashDelay = 3000
        Handler().postDelayed({
            if (pref.isStateLogin()) {
                pb_splashscreen.visibility = View.VISIBLE

                val email = pref.getEmail()
                val token = pref.getToken()
                MailFoxRestClient.setHeader("Authorization", token!!)
                MailFoxRestClient.get("/users/current", null, object: AsyncHttpResponseHandler() {
                    override fun onSuccess(
                        statusCode: Int,
                        headers: Array<out Header>,
                        responseBody: ByteArray
                    ) {
                        pb_splashscreen.visibility = View.INVISIBLE
                        val result = String(responseBody)

                        try {
                            val responseObject = JSONObject(result)
                            val data = responseObject.getJSONObject("data")

                            val user = getDataUser(data.getJSONObject("user"))

                            AllInboxFragment.inboxes = user.inboxMails
                            val intent = Intent(this@SplashScreenActivity, MainActivity::class.java)
                            intent.putExtra(MainActivity.EXTRA_USER, user)
                            startActivity(intent)
                            finish()

                        } catch (e: Exception) {
                            Toast.makeText(this@SplashScreenActivity, e.toString(), Toast.LENGTH_SHORT).show()
                        }
                    }

                    override fun onFailure(
                        statusCode: Int,
                        headers: Array<out Header>,
                        responseBody: ByteArray,
                        error: Throwable
                    ) {
                        pb_splashscreen.visibility = View.INVISIBLE
                        val result = String(responseBody)

                        getResponseErr(this@SplashScreenActivity, result)
                    }

                })
            } else {
                startActivity(Intent(this, LoginActivity::class.java))
                finish()
            }
        }, splashDelay.toLong())
    }
}