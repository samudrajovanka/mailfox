package com.ksmandroid.mailfox.ui.register

import android.content.Intent
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.ksmandroid.mailfox.R
import com.ksmandroid.mailfox.rest.MailFoxRestClient
import com.ksmandroid.mailfox.ui.login.LoginActivity
import com.ksmandroid.mailfox.ui.splashscreen.SplashScreenActivity
import com.ksmandroid.mailfox.utils.getResponseErr
import com.ksmandroid.mailfox.utils.hideKeyboard
import com.loopj.android.http.AsyncHttpResponseHandler
import com.loopj.android.http.RequestParams
import cz.msebera.android.httpclient.Header
import kotlinx.android.synthetic.main.activity_register.*
import org.json.JSONObject
import java.lang.Exception

class RegisterActivity : AppCompatActivity(), View.OnClickListener {
    private var isClickShowPassword = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        initiateUI()
    }

    private fun initiateUI() {
        // menghilangkan toolbar
        supportActionBar?.hide()

        // toggle untuk melihat password
        et_password_register.setOnTouchListener(OnTouchListener { _, event ->
            // Drawable right is 2
            if (event.action == MotionEvent.ACTION_UP) {
                if (event.rawX >= et_password_register.right - et_password_register.compoundDrawables[2].bounds.width()) {
                    if (isClickShowPassword) {
                        et_password_register.transformationMethod =
                            PasswordTransformationMethod.getInstance()
                        et_password_register.setSelection(et_password_register.text!!.length)
                        et_password_register.setCompoundDrawablesRelativeWithIntrinsicBounds(
                            R.drawable.ic_password,
                            0,
                            R.drawable.ic_show_password,
                            0
                        )
                        isClickShowPassword = false
                    } else {
                        et_password_register.transformationMethod =
                            HideReturnsTransformationMethod.getInstance()
                        et_password_register.setSelection(et_password_register.text!!.length)
                        et_password_register.setCompoundDrawablesRelativeWithIntrinsicBounds(
                            R.drawable.ic_password,
                            0,
                            R.drawable.ic_hide_password,
                            0
                        )
                        isClickShowPassword = true
                    }

                    true
                }
            }

            false
        })


        btn_register.setOnClickListener(this)
        tv_to_login.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.btn_register -> {
                hideKeyboard()
                val name = et_name_register.text.toString().trim()
                val email = et_email_register.text.toString().trim()
                val password = et_password_register.text.toString().trim()

                var isValid = true

                if (name.isEmpty()) {
                    et_name_register.error = getString(R.string.error_name_empty)
                    isValid = false
                }

                if (email.isEmpty()) {
                    et_email_register.error = getString(R.string.error_email_empty)
                    isValid = false
                } else {
                    if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                        et_email_register.error = getString(R.string.error_email_not_valid)
                        isValid = false
                    }
                }

                if (password.isEmpty()) {
                    et_password_register.error = getString(R.string.error_password_empty)
                    isValid = false
                } else {
                    if (password.length < 8) {
                        et_password_register.error = getString(R.string.error_password_minimum)
                        isValid = false
                    }
                }

                if (isValid) {
                    pb_register.visibility = View.VISIBLE

                    val params = RequestParams()
                    params.put("fullName", name)
                    params.put("email", email)
                    params.put("password", password)

                    MailFoxRestClient.post(
                        "/auth/register",
                        params,
                        object : AsyncHttpResponseHandler() {
                            override fun onSuccess(
                                statusCode: Int,
                                headers: Array<out Header>,
                                responseBody: ByteArray
                            ) {
                                pb_register.visibility = View.INVISIBLE
                                val result = String(responseBody)

                                try {
                                    val responseObject = JSONObject(result)

                                    val success = responseObject.getBoolean("success")
                                    val message = responseObject.getString("message")

                                    if (success) {
                                        anim_success_register.visibility = View.VISIBLE
                                        anim_success_register.playAnimation()
                                        anim_success_register.addAnimatorUpdateListener { valueAnimator ->
                                            val progress =
                                                (valueAnimator.animatedValue as Float * 100).toInt()

                                            // jika animasi selesai intent ke login activity
                                            if (progress == 99) {

                                                startActivity(
                                                    Intent(
                                                        this@RegisterActivity,
                                                        LoginActivity::class.java
                                                    )
                                                )
                                                finish()
                                            }
                                        }
                                    } else {
                                        Toast.makeText(this@RegisterActivity, message, Toast.LENGTH_SHORT).show()
                                    }

                                } catch (e: Exception) {
                                    print(e)
                                }
                            }

                            override fun onFailure(
                                statusCode: Int,
                                headers: Array<out Header>,
                                responseBody: ByteArray,
                                error: Throwable
                            ) {
                                pb_register.visibility = View.INVISIBLE

                                val result = String(responseBody)

                                getResponseErr(this@RegisterActivity, result)
                            }

                        })

                }

            }
            R.id.tv_to_login -> {
                startActivity(Intent(this, LoginActivity::class.java))
                finish()
            }
        }
    }
}