package com.ksmandroid.mailfox.ui.allInbox

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.ksmandroid.mailfox.R
import com.ksmandroid.mailfox.adapter.EmailAdapter
import com.ksmandroid.mailfox.model.Mail
import com.ksmandroid.mailfox.rest.MailFoxRestClient
import com.ksmandroid.mailfox.sharedpreferences.SharedPrefManager
import com.ksmandroid.mailfox.ui.main.MainActivity
import com.ksmandroid.mailfox.ui.splashscreen.SplashScreenActivity
import com.ksmandroid.mailfox.utils.getDataUser
import com.ksmandroid.mailfox.utils.getResponseErr
import com.loopj.android.http.AsyncHttpResponseHandler
import cz.msebera.android.httpclient.Header
import kotlinx.android.synthetic.main.empty_list.*
import kotlinx.android.synthetic.main.fragment_all_inbox.*
import org.json.JSONObject
import java.util.ArrayList

class AllInboxFragment : Fragment() {

    companion object {
        lateinit var inboxes: ArrayList<Mail?>
    }

    private lateinit var pref: SharedPrefManager
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_all_inbox, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initiateUI()
    }

    override fun onResume() {
        super.onResume()

        pref = SharedPrefManager(requireContext())
        val token = pref.getToken()
        MailFoxRestClient.setHeader("Authorization", token!!)
        MailFoxRestClient.get("/users/current", null, object : AsyncHttpResponseHandler() {
            override fun onSuccess(
                statusCode: Int,
                headers: Array<out Header>,
                responseBody: ByteArray
            ) {
                val result = String(responseBody)

                try {
                    val responseObject = JSONObject(result)
                    val data = responseObject.getJSONObject("data")

                    val user = getDataUser(data.getJSONObject("user"))

                    inboxes = user.inboxMails
                } catch (e: Exception) {
                    Toast.makeText(requireContext(), e.toString(), Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(
                statusCode: Int,
                headers: Array<out Header>,
                responseBody: ByteArray,
                error: Throwable
            ) {
                val result = String(responseBody)

                getResponseErr(requireContext(), result)
            }

        })
    }

    private fun initiateUI() {
        checkInbox()
        showInboxes()
    }

    private fun checkInbox() {
        if (inboxes.size == 0) {
            list_empty.visibility = View.VISIBLE
        } else {
            list_empty.visibility = View.GONE
        }
    }

    private fun showInboxes() {
        rv_all_inbox.setHasFixedSize(true)
        rv_all_inbox.layoutManager = LinearLayoutManager(activity)
        val adapter = EmailAdapter(inboxes)
        rv_all_inbox.adapter = adapter
    }
}