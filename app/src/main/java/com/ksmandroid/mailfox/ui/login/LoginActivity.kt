package com.ksmandroid.mailfox.ui.login

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.MotionEvent
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.ksmandroid.mailfox.R
import com.ksmandroid.mailfox.rest.MailFoxRestClient
import com.ksmandroid.mailfox.sharedpreferences.SharedPrefManager
import com.ksmandroid.mailfox.ui.allInbox.AllInboxFragment
import com.ksmandroid.mailfox.ui.main.MainActivity
import com.ksmandroid.mailfox.ui.register.RegisterActivity
import com.ksmandroid.mailfox.ui.splashscreen.SplashScreenActivity
import com.ksmandroid.mailfox.utils.getDataUser
import com.ksmandroid.mailfox.utils.getResponseErr
import com.ksmandroid.mailfox.utils.hideKeyboard
import com.loopj.android.http.AsyncHttpResponseHandler
import com.loopj.android.http.RequestParams
import cz.msebera.android.httpclient.Header
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_splash_screen.*
import org.json.JSONObject

class LoginActivity : AppCompatActivity(), View.OnClickListener {
    private var isClickShowPassword = false
    private lateinit var pref: SharedPrefManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        initiateUI()
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun initiateUI() {
        // menghilangkan toolbar
        supportActionBar?.hide()

        pref = SharedPrefManager(this)

        // toggle untuk melihat password
        et_password_login.setOnTouchListener { _, event ->
            // Drawable right is 2
            if (event.action == MotionEvent.ACTION_UP) {
                if (event.rawX >= et_password_login.right - et_password_login.compoundDrawables[2].bounds.width()) {
                    if (isClickShowPassword) {
                        et_password_login.transformationMethod =
                            PasswordTransformationMethod.getInstance()
                        et_password_login.setSelection(et_password_login.text!!.length)
                        et_password_login.setCompoundDrawablesRelativeWithIntrinsicBounds(
                            R.drawable.ic_password,
                            0,
                            R.drawable.ic_show_password,
                            0
                        )
                        isClickShowPassword = false
                    } else {
                        et_password_login.transformationMethod =
                            HideReturnsTransformationMethod.getInstance()
                        et_password_login.setSelection(et_password_login.text!!.length)
                        et_password_login.setCompoundDrawablesRelativeWithIntrinsicBounds(
                            R.drawable.ic_password,
                            0,
                            R.drawable.ic_hide_password,
                            0
                        )
                        isClickShowPassword = true
                    }

                    true
                }
            }

            false
        }

        btn_login.setOnClickListener(this)
        tv_to_register.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        when(v.id) {
            R.id.btn_login -> {
                hideKeyboard()
                val email = et_email_login.text.toString().trim()
                val password = et_password_login.text.toString().trim()
                var isValid = true

                if(email.isEmpty()) {
                    et_email_login.error = getString(R.string.error_email_empty)
                    isValid = false
                } else {
                    if(!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                        et_email_login.error = getString(R.string.error_email_not_valid)
                        isValid = false
                    }
                }

                if(password.isEmpty()) {
                    et_password_login.error = getString(R.string.error_password_empty)
                    isValid = false
                }

                if(isValid) {
                    pb_login.visibility = View.VISIBLE

                    val params = RequestParams()
                    params.put("email", email)
                    params.put("password", password)

                    MailFoxRestClient.post("/auth/login", params, object : AsyncHttpResponseHandler() {
                        override fun onSuccess(
                            statusCode: Int,
                            headers: Array<out Header>,
                            responseBody: ByteArray
                        ) {
                            val result = String(responseBody)

                            try {
                                val responseObject = JSONObject(result)

                                val token = responseObject.getString("token")

                                pref.saveStateLogin(true)
                                pref.saveToken(token)
                                pref.saveEmail(email)

                                MailFoxRestClient.setHeader("Authorization", token)
                                MailFoxRestClient.get("/users/current", null, object: AsyncHttpResponseHandler() {
                                    override fun onSuccess(
                                        statusCode: Int,
                                        headers: Array<out Header>,
                                        responseBody: ByteArray
                                    ) {
                                        pb_login.visibility = View.INVISIBLE
                                        val resultUser = String(responseBody)

                                        try {
                                            val responseObjectUser = JSONObject(resultUser)
                                            val data = responseObjectUser.getJSONObject("data")

                                            val user = getDataUser(data.getJSONObject("user"))

                                            AllInboxFragment.inboxes = user.inboxMails
                                            val intent = Intent(this@LoginActivity, MainActivity::class.java)
                                            intent.putExtra(MainActivity.EXTRA_USER, user)
                                            startActivity(intent)
                                            finish()

                                        } catch (e: Exception) {
                                            pref.saveStateLogin(false)
                                            Toast.makeText(this@LoginActivity, e.toString(), Toast.LENGTH_SHORT).show()
                                        }
                                    }

                                    override fun onFailure(
                                        statusCode: Int,
                                        headers: Array<out Header>,
                                        responseBody: ByteArray,
                                        error: Throwable
                                    ) {
                                        pref.saveStateLogin(false)
                                        pb_login.visibility = View.INVISIBLE
                                        val resultUser = String(responseBody)

                                        getResponseErr(this@LoginActivity, resultUser)

                                    }

                                })

                            } catch (e: Exception) {
                                print(e)
                            }

                        }

                        override fun onFailure(
                            statusCode: Int,
                            headers: Array<out Header>,
                            responseBody: ByteArray,
                            error: Throwable
                        ) {
                            pref.saveStateLogin(false)
                            pb_login.visibility = View.INVISIBLE

                            val result = String(responseBody)
                            getResponseErr(this@LoginActivity, result)

                        }

                    })
                }
            }
            R.id.tv_to_register -> {
                startActivity(Intent(this, RegisterActivity::class.java))
                finish()
            }
        }
    }

}