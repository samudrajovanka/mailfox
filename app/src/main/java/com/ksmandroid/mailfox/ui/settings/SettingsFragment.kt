package com.ksmandroid.mailfox.ui.settings

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.ksmandroid.mailfox.R
import com.ksmandroid.mailfox.listener.BottomSheetAlertListener
import com.ksmandroid.mailfox.sharedpreferences.SharedPrefManager
import com.ksmandroid.mailfox.ui.login.LoginActivity
import com.ksmandroid.mailfox.views.BottomSheetAlertFragment
import kotlinx.android.synthetic.main.fragment_settings.*

class SettingsFragment : Fragment(), BottomSheetAlertListener, View.OnClickListener {

    private val btsAlert by lazy { BottomSheetAlertFragment(this) }
    private lateinit var tagBts: String
    private lateinit var pref: SharedPrefManager

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_settings, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initiateUI()
    }

    private fun initiateUI() {
        pref = SharedPrefManager(requireContext())

        container_log_out.setOnClickListener(this)
        container_delete_account.setOnClickListener(this)
    }

    override fun getUserChoices(yes: Boolean) {
        if (yes) {
            if (tagBts.equals("Log Out", true)) {
                pref.saveToken("")
                pref.saveStateLogin(false)
                startActivity(Intent(activity, LoginActivity::class.java))
                activity?.finish()
            } else if (tagBts.equals("Delete Account", true)) {
                pb_settings.visibility = View.VISIBLE

            }
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.container_log_out -> {
                btsAlert.setTitle(getString(R.string.log_out_title))
                btsAlert.setSubtitle(getString(R.string.log_out_alert))
                activity?.supportFragmentManager?.let { btsAlert.show(it, "Log Out") }
                tagBts = "Log Out"
            }
            R.id.container_delete_account -> {
                btsAlert.setTitle(getString(R.string.delete_account_title))
                btsAlert.setSubtitle(getString(R.string.delete_account_alert))
                activity?.supportFragmentManager?.let { btsAlert.show(it, "Delete Account") }
                tagBts = "Delete Account"
            }
        }
    }

}