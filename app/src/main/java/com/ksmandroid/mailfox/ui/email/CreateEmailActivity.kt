package com.ksmandroid.mailfox.ui.email

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.ksmandroid.mailfox.R
import com.ksmandroid.mailfox.model.Mail
import com.ksmandroid.mailfox.rest.MailFoxRestClient
import com.ksmandroid.mailfox.sharedpreferences.SharedPrefManager
import com.ksmandroid.mailfox.utils.getResponseErr
import com.loopj.android.http.AsyncHttpResponseHandler
import com.loopj.android.http.RequestParams
import cz.msebera.android.httpclient.Header
import kotlinx.android.synthetic.main.activity_create_email.*
import kotlinx.android.synthetic.main.toolbar.*
import org.json.JSONObject
import java.lang.Exception

class CreateEmailActivity : AppCompatActivity(), View.OnClickListener {

    companion object {
        const val EXTRA_EMAIL_USER = "extra_email_user"
    }

    private lateinit var emailUser: String
    private lateinit var pref: SharedPrefManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_email)

        initiateUI()
    }

    private fun initiateUI() {
        emailUser = intent.getStringExtra(EXTRA_EMAIL_USER)!!

        // menghilangkan toolbar
        supportActionBar?.hide()

        pref = SharedPrefManager(this)

        btn_toolbar.setImageResource(R.drawable.ic_send_fill)
        tv_from_email_user.text = emailUser

        btn_back_toolbar.setOnClickListener(this)
        btn_toolbar.setOnClickListener(this)

    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.btn_back_toolbar -> {
                onBackPressed()
                finish()
            }
            R.id.btn_toolbar -> {
                val to = et_to_email.text.toString().trim()
                val subject = et_subject_email.text.toString().trim()
                val body = et_body_email.text.toString().trim()
                val mail = Mail(emailUser, to, subject, body)
                pb_create_email.visibility = View.VISIBLE
                sendToUser(mail)
            }
        }
    }

    private fun sendToUser(mail: Mail) {
        val params = RequestParams()
        //params.put("from", mail.from)
        params.put("to", mail.to)
        params.put("subject", mail.subject)
        params.put("message", mail.message)

        val token = pref.getToken()

        MailFoxRestClient.setHeader("Authorization", token!!)
        MailFoxRestClient.post("/users/current/send-mail", params, object: AsyncHttpResponseHandler() {
            override fun onSuccess(
                statusCode: Int,
                headers: Array<out Header>,
                responseBody: ByteArray
            ) {
                val result = String(responseBody)

                pb_create_email.visibility = View.INVISIBLE

                try {
                    Toast.makeText(this@CreateEmailActivity, "Mail was sent successfully",Toast.LENGTH_SHORT).show()
                    onBackPressed()
                    finish()
                } catch (e: Exception) {
                    Toast.makeText(this@CreateEmailActivity, "Please try again",Toast.LENGTH_SHORT).show()
                }

            }

            override fun onFailure(
                statusCode: Int,
                headers: Array<out Header>,
                responseBody: ByteArray,
                error: Throwable
            ) {
                pb_create_email.visibility = View.INVISIBLE
                Toast.makeText(this@CreateEmailActivity, "Please try again",Toast.LENGTH_SHORT).show()
            }

        })
    }

}